#!/bin/bash

# añadir registro en LDAP
function addRegister(){
	file=$1
	read -sp "Introduzca la contraseña de administrador de LDAP: " pass
	ldapadd -x -D cn=admin,dc=example,dc=com -f "$file" -w $pass
}

function delRegister(){
	file=$1
	uid=$2
	ou=$3
	read -sp "Introduzca la contraseña de administrador de LDAP: " pass
	ldapdelete -D cn=admin,dc=example,dc=org -w curso "uid=$uid,ou=$ou,dc=example,dc=com"
}

# funcion de busqueda de shells
function searchShells(){
	if [ -f "/etc/shells" ];
	then
		cat "/etc/shells" | grep -v "#"
	else
		echo "/bin/bash"
	fi
}

# funcion de busqueda de shells por numero
function searchAndFindShell(){
	if [ -f "/etc/shells" ];
	then
		cat "/etc/shells" | grep -v "#" | head -n$1 | tail -n1
	else
		echo "/bin/bash"
	fi
}

# funcion de busqueda de unidades administrativas
function searchUA(){
	ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com ou | grep ou | grep -v dn | sed 's/ou: //g'
}

# funcion de busqueda de unidades administrativas
function searchG(){
	ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com cn | grep "cn: " | sed 's/cn: //g'
}

# funcion de busqueda de unidades administrativas
function searchAndFindUA(){
	ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com ou | grep ou | grep -v dn | sed 's/ou: //g' | head -n$1 | tail -n1
}

# funcion de busqueda de unidades grupos
function searchAndFindG(){
	ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com cn | grep "cn: " | sed 's/cn: //g' | head -n$1 | tail -n1
}

# funcion de busqueda de unidades administrativas
function searchAndFindGroupNumber(){
	ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com gidNumber | grep gidNumber | sed 's/gidNumber: //g' | head -n$1 | tail -n1
}

# funcion de busqueda de numero de grupo
function searchGidNumber(){
	s=$(ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com gidNumber | grep gidNumber | sed 's/gidNumber: //g' | sort -nr | head -n1)
	if [ $s -ge 4999 ];
	then
		echo "$(( $s + 1 ))"
	else
		echo "5000"
	fi
}

# funcion de busqueda de numero de grupo
function searchUidNumber(){
        s=$(ldapsearch -x -LLL -H ldapi:/// -b dc=example,dc=com uidNumber | grep uidNumber | sed 's/uidNumber: //g' | sort -nr | head -n1)
        if [ "$s" != "" ] && [ $s -ge 9999 ];
        then
                echo "$(( $s + 1 ))"
        else
                echo "10000"
        fi
}

# funcion de creacion de plantilla de unidad administrativa
function createUATemplate(){
	if [ ! -f ".unit" ];
	then
		echo "dn: ou={ou},dc=example,dc=com" > .unit
		echo "objectClass: organizationalUnit" >> .unit
		echo "ou: {ou}" >> .unit
	fi
}

# funcion de creacion de plantilla de grupo
function createGTemplate(){
    if [ ! -f ".group" ];
    then
        echo "dn: cn={cn},ou={ua},dc=example,dc=com" > .group
        echo "objectClass: posixGroup" >> .group
        echo "cn: {cn}" >> .group
        echo "gidNumber: {gidn}" >> .group
    fi
}

# funcion de sustitucion de plantilla de usuario
function createUTemplate(){
    if [ ! -f ".user" ];
    then
        	echo "dn: uid={uid},ou={ou},dc=example,dc=com" > .user
        	echo "objectClass: inetOrgPerson" >> .user
	        echo "objectClass: posixAccount" >> .user
        	echo "objectClass: shadowAccount" >> .user
	        echo "uid: {uid}" >> .user
        	echo "sn: {sn}" >> .user
	        echo "givenName: {n}" >> .user
        	echo "cn: {n} {sn}" >> .user
	        echo "displayName: {n} {sn}" >> .user
        	echo "uidNumber: {uidn}" >> .user
	        echo "gidNumber: {gidn}" >> .user
	        echo "userPassword: {pwd}" >> .user
        	echo "gecos: {n} {sn}" >> .user
	        echo "loginShell: {sh}" >> .user
        	echo "homeDirectory: /home/{home}" >> .user
    fi
}

# funcion de sustitucion de variables en archivo de plantilla
function replace(){
    createUTemplate
	#replace .user "$uid" "$ou" "$n" "$sn" "$pwd" "$sh" "$home" "$(echo "$uidn,$gidn,${HOME}${f}.ldif")"
    file=$1
    uid=$2
    ou=$3
    n=$4
    sn=$5
    pwd=$6
    sh=$7
    home=$8
    uidn=$(echo "$9" | cut -d',' -f1)
    gidn="$(echo "$9" | cut -d',' -f2)"
    result=$(echo "$9" | cut -d',' -f3)
    echo "Los valores de entrada son $1 $2 $3 $4 $5 $6 $7 $8 $uidn $home $result"
    # replace .user "$uid" "$ou" "$n" "$sn" "$pwd" "$sh" "$home" "$(echo "$uidn,$gidn,${HOME}${f}.ldif")"
    cp "$file" "$result"
    sed -i s~{uid}~$uid~g $result
    sed -i s~{ou}~$ou~g $result
    sed -i s~{n}~$n~g $result
    sed -i s~{sn}~$sn~g $result
    sed -i s~{pwd}~$pwd~g $result
    sed -i s~{sh}~$sh~g $result
    sed -i s~{home}~$home~g $result # error
    sed -i s~{gidn}~$gidn~g $result
    sed -i s~{uidn}~$uidn~g $result
}

# sustitucion de archivo temporal de grupo
function replaceGroup(){
    createGTemplate
    file=$1
    ua=$3
    cn=$2
    gidn=$4
    result=$5
    cp "$file" "$result"
    sed -i s/{ua}/$ua/g $result
    sed -i s/{cn}/$cn/g $result
    sed -i s/{gidn}/$gidn/g $result
}

function replaceUnit(){
	createUATemplate
	file=$1
	ou=$2
	result=$3
	cp $file $result
	sed -i s/{ou}/$ou/g $result
}

function newGroup(){
	echo ""
	read -p "Introduzca CN: " cn
	# read -p "Introduzca el número de grupo: " gidn
	echo "Escoja OU: "
	num=1
	for i in $(searchUA);
	do
		echo "$num - $i"
		num=$(( $num + 1 ))	
	done
	read choose
	ua=$(searchAndFindUA $choose)
	if [ "$ua" = "" ];
	then
		echo "El número es incorrecto"
		newGroup
	fi
	gidn="$(searchGidNumber)"
	echo ""
	echo "Los datos son: "
	echo ""
	echo "CN: $cn"
	echo "UA: $ua"
	echo "GIDN: $gidn"
	echo ""
	read -p "¿Son correctos los datos introducidos? s/n " yesno
        echo ""
        if [ "${yesno^^}" = "S" ];
        then
                # guardado de datos
                f=$(date +%Y%m%d%N)
                replaceGroup .group $cn $ua $gidn ${HOME}${f}.ldif
                addRegister "${HOME}${f}.ldif"
                if [ $? -eq 0 ];
                    then
                        echo "Se ha insertado correctamente la nueva unidad"
                        menu
                else
                    echo "Ha habido un error en la inserción, o la contraseña es incorrecta"
                    exit 1
                fi
        else
                # salir
                echo "Fin de la ejecución"
                exit 0
        fi
}

# funcion de creacion de unidad administrativa
function newUnit(){
	# añadir nueva unidad administrativa
	echo ""
	read -p "Introduzca el nombre de la unidad organizativa: " ou
	echo ""
	echo "Los datos son: "
	echo ""
	echo "Unidad organizativa: $ou"
	echo ""
	read -p "¿Son correctos los datos introducidos? s/n " yesno
	echo ""
	if [ "${yesno^^}" = "S" ];
	then
		# guardado de datos
                f=$(date +%Y%m%d%N)
                replaceUnit .unit $ou ${HOME}${f}.ldif
		addRegister "${HOME}${f}.ldif"
            if [ $? -eq 0 ];
                then
                    echo "Se ha insertado correctamente la nueva unidad"
                    menu
            else
                echo "Ha habido un error en la inserción, o la contraseña es incorrecta"
                exit 1
            fi
        else
            # salir
            echo "Fin de la ejecución"
            exit 0
	fi
}

function newUser() {
	# añadir nuevo usuario
	echo ""
		echo "Escoja Unidad Organizativa: "
	num=1
	for i in $(searchUA);
	do
		echo "$num - $i"
		num=$(( $num + 1 ))	
	done
	read choose
	ou=$(searchAndFindUA $choose)
	if [ "$ou" = "" ];
	then
		echo "El número es incorrecto"
		newUser
	fi
	# busqueda de Grupo
	echo ""
	echo "Escoge un grupo: "
	num=1
	for i in $(searchG);
	do
		echo "$num - $i"
		num=$(( $num + 1 ))	
	done
	read choose
	gidn=$(searchAndFindGroupNumber $choose)
	if [ "$ou" = "" ];
	then
		echo "El número es incorrecto"
		newUser
	fi
	echo ""
	echo "Escoge una shell válida"
	num=1
	for i in $(searchShells);
	do
		echo "$num - $i"
		num=$(( $num + 1 ))
	done
	read choose
	sh=$(searchAndFindShell $choose)
	if [ "$sh" = "" ];
        then
                echo "El número es incorrecto"
                newUser
        fi
	uidn="$(searchUidNumber)"
        read -p "Introduzca el uid del usuario: " uid
        # read -p "Introduzca el ou del usuario: " ou
        read -p "Introduzca el nombre del usuario: " n
        read -p "Introduzca el apellido del usuario: " sn
        read -p "Introduzca la contraseña del usuario: " pwd
        # read -p "Introduzca la shell por defecto del usuario: " sh
        read -p "Introduzca el home directory del usuario /home/?: " home
        echo ""
        echo "Los datos son: "
        echo ""
        echo "UID: $uid"
        echo "OU: $ou"
        echo "Nombre: $n"
        echo "Apellidos: $sn"
        echo "Shell: $sh"
        echo "Home: $home"
	echo "GIDN: $gidn"
	echo "UIDN: $uidn"
        echo ""
        read -p "¿Son correctos estos datos? s/n: " yesno
	echo ""
	if [ "${yesno^^}" = "S" ];
        then
        	# guardado de datos
                f=$(date +%Y%m%d%N)
		echo "F vale: $f y home $HOME"
		replace .user "$uid" "$ou" "$n" "$sn" "$pwd" "$sh" "$home" "$(echo "$uidn,$gidn,${HOME}${f}.ldif")"
		echo "Los valores son replace .user|$uid|$ou|$n|$sn|$pwd|$sh|$home|$(echo \"$uidn,$gidn,${HOME}${f}.ldif\")"
		# read -p "Introduzca la contraseña de administrador de LDAP: " pass
		addRegister "${HOME}${f}.ldif"
		if [ $? -eq 0 ];
		    then
			echo "Se ha insertado correctamente el nuevo usuario"
			exit 0
		else
		    echo "Ha habido un error en la inserción, o la contraseña es incorrecta"
		    exit 1
		fi
		else
		# salir
		echo "Fin de la ejecución"
		exit 0
	fi
}

function menu(){
HOME="/etc/ldap/slapd.d/"
templateUA=".unit"
templateU=".user"
templateG=".group"

echo ""
echo "############## BIENVENIDO AL GESTOR DE LDAP ###############"
echo ""
#if [ "${yesno^^}" = "S" ];
#    then
        echo ""
	read -p "¿Desea añadir una Unidad administrativa (A), Grupo(G) o Usuario(U)?: " resp
        if [ "${resp^^}" = "G" ];
        then
		newGroup
		exit 0
            else
                if [ "${resp^^}" = "U" ];
                then
			# añadir un nuevo usuario
			newUser
                else
			if [ "${resp^^}" = "A" ];
			then
				newUnit
			else
                    		echo "La respuesta no es correcta, fin de la ejecución"
				exit 0
			fi	
            fi
        fi
#    else
#        echo ""
#        exit 0
#fi
}

menu
